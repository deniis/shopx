import 'package:get/get.dart';
import 'package:shopx/models/product.dart';
import 'package:shopx/services/remote_service.dart';

class ProductController extends GetxController {
  var productList = List<Product>.empty().obs;
  var isLoading = true.obs;

  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    isLoading(true);
    var products = await RemoteService.fetchProducts();
    if (products != null) productList.value = products;
    isLoading(false);
  }
}
